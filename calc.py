#!/usr/bin/python3

import sys
import operator


def Calculadora(funcion, operando1, operando2):

    operaciones = {"suma": operator.add, "resta": operator.sub,
                   "multiplicacion": operator.mul,
                   "division": operator.truediv}

    if funcion in operaciones:
        return operaciones[funcion](operando1, operando2)
    else:
        print('Operaciones disponibles: suma, resta, multiplicacion,\
         division')


def main():

    print('A continuación se muestran las sumas y restas indicadas:')
    suma1 = Calculadora("suma", 1, 2)
    print('\nLa suma de 1 + 2 es: ' + str(suma1))
    suma2 = Calculadora("suma", 3, 4)
    print('\nLa suma de 3 + 4 es: ' + str(suma2))
    resta1 = Calculadora("resta", 6, 5)
    print('\nLa resta de 6 - 5 es: ' + str(resta1))
    resta2 = Calculadora("resta", 8, 7)
    print('\nLa resta de 8 - 7 es: ' + str(resta2))


if __name__ == "__main__":

    try:
        main()
    except ZeroDivisionError:
        print('Division entre 0 no soportada')
    except ValueError:
        print("Por favor, introduzca como operadores valores numéricos")
